import numpy as np
 
a = np.loadtxt(open("data.csv", "rb"), delimiter=",", skiprows=1)

# parameters:
#    slw  :   sliding window size
#    nupa :   number of co-occureeances of words within the same window

nupa = 3
slw  = 5

d = np.shape(a[0])

print d

# output table with size equal to sliding windows
b = np.zeros((d[0]-slw))

# sliding window
for x in range(0, (d[0]-slw)):
      
  print x 

  sum = 0
  # for all nupa words
  for z in range(0, nupa):

    print "Z:"  
    print z

    print a[z][x]   # z is the nupa word, x is the left pointer in the window

    # for all points in the window
    for y in range(x, (x+slw)):

      #print "."
       #print y,z,x
      print (a[z][y])
      if ((a[z][y]) > 0):
         sum = sum + 1

  if (sum>nupa):
      b[x]=1
  else:
      b[x]=0
       #print np.argwhere((a[0] != 0) & (a[1] != 0))

  print b

  np.savetxt('output.txt',b,delimiter=',',newline='\n')